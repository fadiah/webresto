const menu = [
				{
					gambarMakanan: "gambar/menu2.png",
					namaMakanan: "Family Combo",
					hargaMakanan: 140000,
				 	deskripsiMakanan: "Paket Keluarga"
				}, {
					gambarMakanan: "gambar/menu4.png",
					namaMakanan: "Chicken Burger",
					hargaMakanan: 30000,
					deskripsiMakanan: "Burger Ayam"
				}, {
					gambarMakanan: "gambar/menu6.png",
					namaMakanan: "Friends Combo",
					hargaMakanan: 50000,
					deskripsiMakanan: "Paket Teman"
				},{
					gambarMakanan: "gambar/menu8.png",
					namaMakanan: "Strawberry Iced",
					hargaMakanan: 25000,
					deskripsiMakanan: "Ice Strawberry"
				},{
					gambarMakanan: "gambar/menu10.png",
					namaMakanan: "Pom Pom",
					hargaMakanan: 25000,
					deskripsiMakanan: "Pom Pom"
				},{
					gambarMakanan: "gambar/menu12.png",
					namaMakanan: "Super Breakfast",
					hargaMakanan: 30000,
					deskripsiMakanan: "Paket Sarapan"
				},{
					gambarMakanan: "gambar/menu1.png",
					namaMakanan: "9 pcs Bucket",
					hargaMakanan: 140000,
					deskripsiMakanan: "Paket Ayam"
				},{
					gambarMakanan: "gambar/menu3.png",
					namaMakanan: "Chicken Wings Bucket",
					hargaMakanan: 120000,
					deskripsiMakanan: "Paket Sayap Ayam"
				},{
					gambarMakanan: "gambar/menu5.png",
					namaMakanan: "Young Combo",
					hargaMakanan: 30000,
					deskripsiMakanan: "Paket Remaja"
				},{
					gambarMakanan: "gambar/menu7.png",
					namaMakanan: "Chocolate Iced",
					hargaMakanan: 25000,
					deskripsiMakanan: "Ice Coklat"
				},{
					gambarMakanan: "gambar/menu9.png",
					namaMakanan: "Lemon Tea Iced",
					hargaMakanan: 25000,
					deskripsiMakanan: "Ice Lemon"
				},{
					gambarMakanan: "gambar/menu11.png",
					namaMakanan: "Scramble Egg",
					hargaMakanan: 25000,
					deskripsiMakanan: "Telur Acak"
				}
			];


			const body = document.querySelector("body");
			for(let data of menu){
				body.innerHTML +=`
							<div class="container" style="margin-right:1px;">
						        <div class="row" style="float: left;">
						            <div class="col-sm m-2" >
						                <div class="card m-3" style="width: 18rem;">
						                    <img src="${data.gambarMakanan}" class="card-img-top" alt="...">
						                    <div class="card-body">
						                        <h4 class="card-title">${data.namaMakanan}</h4>
						                        <h5>Rp.${data.hargaMakanan}</h5><br>
						                        <p class="card-text" style="text-align:center;">${data.deskripsiMakanan}</b></p><br>
						                        <a href="#" class="btn" style="background-color:red; color: white;">Buy Now</a>
						                    </div>
						                </div>
						            </div>
						        </div>
						     </div>`;	
								}
	